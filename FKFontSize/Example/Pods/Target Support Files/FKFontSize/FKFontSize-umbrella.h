#import <UIKit/UIKit.h>

#import "NSAttributedString+FKFontSize.h"

FOUNDATION_EXPORT double FKFontSizeVersionNumber;
FOUNDATION_EXPORT const unsigned char FKFontSizeVersionString[];

