//
//  main.m
//  FKFontSize
//
//  Created by nezhelskoy on 11/23/2015.
//  Copyright (c) 2015 nezhelskoy. All rights reserved.
//

@import UIKit;
#import "FKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FKAppDelegate class]));
    }
}
