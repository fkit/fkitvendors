//
//  NSAttributedString+FKFontSize.h
//  FaithKit
//
//  Created by Developer on 11/05/15.
//  Copyright (c) 2015 QiZhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSAttributedString (FKFontSize)

- (NSAttributedString *)attributedStringWithFontSize:(CGFloat)fontSize;

@end
