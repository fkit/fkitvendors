//
//  NSAttributedString+FKFontSize.m
//  FaithKit
//
//  Created by Developer on 11/05/15.
//  Copyright (c) 2015 QiZhang. All rights reserved.
//

#import "NSAttributedString+FKFontSize.h"

@implementation NSAttributedString (FKFontSize)

- (NSAttributedString *)attributedStringWithFontSize:(CGFloat)fontSize
{
    NSMutableAttributedString* attributedString = [self mutableCopy];
    {
        [attributedString beginEditing];
        [attributedString enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, attributedString.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop)
        {
            UIFont *font = value;
            font = [font fontWithSize:fontSize];
            [attributedString removeAttribute:NSFontAttributeName range:range];
            [attributedString addAttribute:NSFontAttributeName value:font range:range];
        }];
        
        [attributedString endEditing];
    }
    
    return [attributedString copy];
}

@end
